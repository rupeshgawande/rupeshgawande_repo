package com.qmetry.qaf.example.components;

import java.util.List;

import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebComponent;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;

public class ComponentClass extends QAFWebComponent {

	@FindBy(locator = "css=.ng-binding")
	private QAFWebElement name;
	@FindBy(locator = "css=ul>li")
	private List<QAFWebElement> subMenuNevigation;
	
	public ComponentClass(String locator) {
		super(locator);
	}

	public QAFWebElement getName() {
		return name;
	}

	public List<QAFWebElement> getSubMenuNevigation() {
		return subMenuNevigation;
	}

	
}
