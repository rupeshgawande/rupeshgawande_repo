package com.qmetry.qaf.example.pages;

import java.util.List;

import javax.swing.JOptionPane;

import org.openqa.selenium.By;

import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.example.components.ComponentClass;
import com.qmetry.qaf.example.utilities.Utilities;

public class ExpenceReimbursmentPage extends WebDriverBaseTestPage<WebDriverTestPage> {

	Utilities util=new Utilities();
	
	
	@FindBy(locator = "homepage.nevigation.button.loc")
	private QAFWebElement nevigationButton;

	@FindBy(locator = "home.Nevigation.Parent.loc")
	private List<ComponentClass> parentMenu;

	@FindBy(locator = "home.Nevigation.Child.loc")
	private List<ComponentClass> childMenu;
	
	@FindBy(locator = "homepage.Reiburstment.button.loc")
	private QAFWebElement reiburstmentButton;

	@FindBy(locator = "homepage.NewExpence.button.loc")
	private QAFWebElement newExpenceButton;

	@FindBy(locator = "homepage.BrowseButton.loc")
	private QAFWebElement browseButton;

	
	@FindBy(locator = "homepage.submitButton.loc")
	private QAFWebElement submitButton;
	
	
	
	public QAFWebElement getNevigationButton() {
		return nevigationButton;
	}

	/*
	 * public List<ComponentClass> getParentMenu() { return parentMenu; }
	 */

	public QAFWebElement getSubmitButton() {
		return submitButton;
	}

	public QAFWebElement getNewExpenceButton() {
		return newExpenceButton;
	}

	public QAFWebElement getBrowseButton() {
		
		return browseButton;
	}

	@Override
	protected void openPage(PageLocator locator, Object... args) {

	}

	public List<ComponentClass> getList() {
		return parentMenu;
	}

	public void clickOnMenuAndSubMenu(String menu, String subMenu) {

		List<ComponentClass> actualList = getList();
		Utilities utilities = new Utilities();
		for (ComponentClass actualMenu : actualList) {
	//	util.scrollUptoElement(reiburstmentButton);
	//	util.scrollUptoElement(reiburstmentButton);
			if (actualMenu.getName().getText().trim().equalsIgnoreCase(menu)) {
				utilities.clickUsingJavaScript(actualMenu.findElement(By.tagName("i")));
				
				List<QAFWebElement> actualSubMenus = actualMenu.getSubMenuNevigation();
				for (QAFWebElement actualSubMenu : actualSubMenus) {
					//utilities.scrollDownFull();
					if (actualSubMenu.findElement(By.tagName("a")).getText().trim().equalsIgnoreCase(subMenu)) {
						utilities.clickUsingJavaScript(actualSubMenu.findElement(By.tagName("a")));
					}
				}
			}
		}
	}

}
