package com.qmetry.qaf.example.pages;

import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;

public class HomePage extends WebDriverBaseTestPage<WebDriverTestPage> {

	
	@FindBy(locator = "homepage.nevigation.button.loc")
	private QAFWebElement nevigationButton;
	
	@FindBy(locator = "btn.Logout.img.loc")
	private QAFWebElement logOutIconButon;

	public QAFWebElement getNevigationButton() {
		return nevigationButton;
	}

	@FindBy(locator = "btn.ManagerView.switch.loc")
	private QAFWebElement manageralViewSwitchButton;

	@Override
	protected void openPage(PageLocator arg0, Object... arg1) {

	}

	public QAFWebElement getLogOutIconButon() {
		return logOutIconButon;
	}

	public QAFWebElement getManageralViewSwitchButton() {
		return manageralViewSwitchButton;
	}

}
