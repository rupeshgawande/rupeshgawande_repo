package com.qmetry.qaf.example.pages;

import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;

public class LeaveApplyHolidayPage extends WebDriverBaseTestPage<WebDriverTestPage> {

	@FindBy(locator = "text.leavevValue.myLeaveList.loc")
	private QAFWebElement leaveValueText;

	
	public QAFWebElement getLeaveValueText() {
		return leaveValueText;
	}



	@Override
	protected void openPage(PageLocator locator, Object... args) {
		// TODO Auto-generated method stub
		
	}

}
