package com.qmetry.qaf.example.pages;

import java.util.List;
import org.hamcrest.Matchers;
import org.openqa.selenium.By;

import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.automation.util.Validator;

public class TeamLeaveListPage extends WebDriverBaseTestPage<WebDriverTestPage> {

	
	@FindBy(locator = "btn.ManagerView.switch.loc")
	private QAFWebElement managerViewSwitch;

	@FindBy(locator = "homepage.nevigation.button.loc")
	private QAFWebElement nevigationButton;

	@FindBy(locator = "subMenuLink.TeamlLeaveList.login.loc")
	private QAFWebElement teamlLeaveList;

	@FindBy(locator = "subMenu.teamleavelist.travel.page.loc")
	private List<QAFWebElement> tabTeamLeaveList;

	public QAFWebElement getTeamlLeaveList() {
		return teamlLeaveList;
	}

	public QAFWebElement getNevigationButton() {
		return nevigationButton;
	}

	public QAFWebElement getManagerViewSwitch() {
		return managerViewSwitch;
	}

	@Override
	protected void openPage(PageLocator locator, Object... args) {
		// TODO Auto-generated method stub

	}

}
