package com.qmetry.qaf.example.pages;

import org.openqa.selenium.By;

import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;

public class TravelRequestPage extends WebDriverBaseTestPage<WebDriverTestPage> {
	@FindBy(locator = "btn.ManagerView.switch.loc")
	private QAFWebElement switchView;

	@FindBy(locator = "btn.travelRequest.click.loc")
	private QAFWebElement travelRequest;

	
	
	@FindBy(locator = "moveToArea.TravelRequests.loc")
	private QAFWebElement moveToAreaTravelRequests;
	
	
	public QAFWebElement getMoveToAreaTravelRequests() {
		return moveToAreaTravelRequests;
	}
	@FindBy(locator = "btn.ViewEye.click.loc")
	private QAFWebElement viewEyeButton;

	@FindBy(locator = "text.TeamlLeaveList.loc")
	private QAFWebElement teamlLeaveList;

	@FindBy(locator = "link.ViewAll.loc")
	private QAFWebElement viewAllButton;
	
	@FindBy(locator = "link.ViewAllRefreshed.loc")
	private QAFWebElement viewAllButtonAfterRefresh;
	

	@FindBy(locator = "button.ExpenseReq.loc")
	private QAFWebElement buttonExpenseReq;

	@FindBy(locator = "text.TeamReimbursementList.loc")
	private QAFWebElement teamReimbursementList;

	@FindBy(locator = "text.TravelRequests.loc")
	private QAFWebElement travelRequestText;
	
	@FindBy(locator = "button.AllButtons.loc")
	private QAFWebElement allButtons;
	
	

	public QAFWebElement getAllButtons() {
		return allButtons;
	}

	@Override
	protected void openPage(PageLocator locator, Object... args) {
		// TODO Auto-generated method stub

	}

	public QAFWebElement getSwitchView() {
		return switchView;
	}

	public QAFWebElement getTravelRequest() {
		return travelRequest;
	}

	public QAFWebElement getTeamlLeaveList() {
		return teamlLeaveList;
	}

	public QAFWebElement getViewAllButton() {
		return viewAllButton;
	}

	public QAFWebElement getButtonExpenseReq() {
		return buttonExpenseReq;
	}

	public QAFWebElement getTeamReimbursementList() {
		return teamReimbursementList;
	}

	public QAFWebElement getTravelRequestText() {
		return travelRequestText;
	}

	public QAFWebElement getViewEyeButton() {
		return viewEyeButton;
	}
   public void getNevigateBack() 
   {
	   driver.navigate().back();
   }
   public void refresh() 
   {
	   driver.navigate().refresh();
   }
  
  
}
