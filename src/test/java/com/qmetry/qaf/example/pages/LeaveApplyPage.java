package com.qmetry.qaf.example.pages;

import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;

public class LeaveApplyPage extends WebDriverBaseTestPage<WebDriverTestPage> {

	@FindBy(locator = "button.Leave.loc")
	private QAFWebElement leaveButton;

	@FindBy(locator = "button.ApplyLeaveMenu.loc")
	private QAFWebElement applyLeaveMenu;

	@FindBy(locator = "button.Apply.loc")
	private QAFWebElement applyButton;
	
	/*@FindBy(locator = "date.beforeXpath.loc")
	private QAFWebElement beforeXpathEle;
	@FindBy(locator = "date.afterXpath.loc")
	private QAFWebElement afterXpathEle;*/
	
//dropDown list	
	@FindBy(locator = "dropdown.box.loc")
	private QAFWebElement dropDownBox;
	
	@FindBy(locator = "dropdown.image.loc")
	private QAFWebElement dropdownImage;
	

    public String beforeXpath=".//*[@id='angular_middle']/div[1]/div/div/div[2]/div/div[1]/div[4]/div[1]/div/div[2]/div/ul/li/div/table/tbody/tr[";
    public String afterXpath="]/td[";
	
    
    public QAFWebElement getDropdownImage() {
		return dropdownImage;
	}

	
	public QAFWebElement getDropDown() {
		return dropDownBox;
	}

	@Override
	protected void openPage(PageLocator locator, Object... args) {
		// TODO Auto-generated method stub
	}

	public QAFWebElement getNevigationArrow() {
		return leaveButton;
	}

	public QAFWebElement getApplyLeaveMenu() {
		return applyLeaveMenu;
	}

	public QAFWebElement getApply() {
		return applyButton;
	}

}
