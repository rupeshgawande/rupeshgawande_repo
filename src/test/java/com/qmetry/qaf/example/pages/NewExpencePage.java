package com.qmetry.qaf.example.pages;

import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;

public class NewExpencePage extends WebDriverBaseTestPage<WebDriverTestPage> {
	@Override
	protected void openPage(PageLocator locator, Object... args) {
		// TODO Auto-generated method stub

	}

	@FindBy(locator = "homepage.nevigation.button.loc")
	private QAFWebElement nevigationButton;

	public QAFWebElement getNevigationButton() {
		return nevigationButton;
	}

	@FindBy(locator = "homepage.NewExpence.button.loc")
	private QAFWebElement newExpenceButton;
	
	
	public QAFWebElement getNewExpenceButton() {
		return newExpenceButton;
	}

	@FindBy(locator = "title.textbox.loc")
	private QAFWebElement titleTextBox;
	

	@FindBy(locator = "project.selectBox.loc")
	private QAFWebElement projectSelectBox;
	

	public QAFWebElement getProjectSelectBox() {
		return projectSelectBox;
	}

	@FindBy(locator = "project.TextBox.loc")
	private QAFWebElement projectTextBox;

	@FindBy(locator = "expencCat.selectBOX.loc")
	private QAFWebElement expencCatSelectBOX;

	@FindBy(locator = "currency.textBox.loc")
	private QAFWebElement currencyTextBox;

	@FindBy(locator = "submit.button.loc")
	private QAFWebElement submitButton;

	@FindBy(locator = "color.calender.loc")
	private QAFWebElement colorCalender;

	public QAFWebElement getTitleTextBox() {
		return titleTextBox;
	}

	public QAFWebElement getProjectTextBox() {
		return projectTextBox;
	}

	public QAFWebElement getExpencCatSelectBOX() {
		return expencCatSelectBOX;
	}

	public QAFWebElement getCurrencyTextBox() {
		return currencyTextBox;
	}

	public QAFWebElement getSubmitButton() {
		return submitButton;
	}

	public QAFWebElement getColorCalender() {
		return colorCalender;
	}

}
