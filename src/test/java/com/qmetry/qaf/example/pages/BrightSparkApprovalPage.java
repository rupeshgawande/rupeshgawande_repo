package com.qmetry.qaf.example.pages;

import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;

public class BrightSparkApprovalPage extends WebDriverBaseTestPage<WebDriverTestPage> {

	@Override
	protected void openPage(PageLocator locator, Object... args) {
		// TODO Auto-generated method stub

	}

	@FindBy(locator = "Menu.Nominate.loc")
	private QAFWebElement nominateMenu;

	@FindBy(locator = "Menu.BrightSpark.loc")
	private QAFWebElement brightSpark;

	@FindBy(locator = "Card.FirstCard.loc")
	private QAFWebElement cardFirst;

	@FindBy(locator = "dropdown.Nominee.loc")
	private QAFWebElement dropdownNominee;

	@FindBy(locator = "dropdown.Project.loc")
	private QAFWebElement dropdownProject;

	@FindBy(locator = "Button.Logout.img.loc")
	private QAFWebElement ButtonLogoutImage;

	@FindBy(locator = "Nest.NAVIGATION.RRRequestsUsingManagerTab")
	private QAFWebElement rRRequestsMenu;

	@FindBy(locator = "TableData.brightsparkRequest.loc")
	private QAFWebElement tableDataBrightSpark;

	@FindBy(locator = "	Button.Approve.loc")
	private QAFWebElement 	buttonApprove;
	
	public QAFWebElement getButtonApprove() {
		return buttonApprove;
	}

	@FindBy(locator = "TextArea.Text.loc")
	private QAFWebElement textAreaText;
	
	@FindBy(locator = "Button.Submitt.loc")
	private QAFWebElement buttonSubmit;
	

	
	
	
	public QAFWebElement getTextAreaText() {
		return textAreaText;
	}

	public QAFWebElement getButtonSubmit() {
		return buttonSubmit;
	}

	public QAFWebElement getTableDataBrightSpark() {
		return tableDataBrightSpark;
	}

	public QAFWebElement getrRRequestsMenu() {
		return rRRequestsMenu;
	}

	public QAFWebElement getButtonLogoutImage() {
		return ButtonLogoutImage;
	}

	public QAFWebElement getDropdownNominee() {
		return dropdownNominee;
	}

	public QAFWebElement getDropdownProject() {
		return dropdownProject;
	}

	@FindBy(locator = "TextArea.ChalngeSituation.loc")
	private QAFWebElement textAreaChalngeSituation;

	@FindBy(locator = "TextArea.SolutionsProvided.loc")
	private QAFWebElement textAreaSolutionsProvided;

	@FindBy(locator = "TextArea.BenefitsOcured.loc")
	private QAFWebElement textAreaBenefitsOcured;

	@FindBy(locator = "TextArea.Rationale.loc")
	private QAFWebElement textAreaRationale;

	@FindBy(locator = "Button.Post.loc")
	private QAFWebElement buttonPost;

	@FindBy(locator = "Button.Approve.loc")
	private QAFWebElement approveButton;

	@FindBy(locator = "Button.Reject.loc")
	private QAFWebElement rejectButton;

	public QAFWebElement getNominateMenu() {
		return nominateMenu;
	}

	public QAFWebElement getBrightSpark() {
		return brightSpark;
	}

	public QAFWebElement getCardFirst() {
		return cardFirst;
	}

	public QAFWebElement getTextAreaChalngeSituation() {
		return textAreaChalngeSituation;
	}

	public QAFWebElement getTextAreaSolutionsProvided() {
		return textAreaSolutionsProvided;
	}

	public QAFWebElement getTextAreaBenefitsOcured() {
		return textAreaBenefitsOcured;
	}

	public QAFWebElement getTextAreaRationale() {
		return textAreaRationale;
	}

	public QAFWebElement getButtonPost() {
		return buttonPost;
	}

	public QAFWebElement getApproveButton() {
		return approveButton;
	}

	public QAFWebElement getRejectButton() {
		return rejectButton;
	}

}
