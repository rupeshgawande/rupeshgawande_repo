package com.qmetry.qaf.example.pages;

import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;

public class NestLoginPage extends WebDriverBaseTestPage<WebDriverTestPage> {
	@FindBy(locator = "login.username.txt.loc")
	private QAFWebElement userNameTextBox;

	@FindBy(locator = "login.password.txt.loc")
	private QAFWebElement passwordTextBox;

	@FindBy(locator = "login.login.btn.loc")
	private QAFWebElement loginButton;

	@Override
	protected void openPage(PageLocator arg0, Object... arg1) {
		driver.get("/");

	}

	/*
	 * public void enterUserNameAndPassword(String userName, String password) {
	 * userNameTextBox.sendKeys(userName); passwordTextBox.sendKeys(password);
	 * loginButton.click();
	 * 
	 * }
	 */

	public QAFWebElement getUserNameTextBox() {
		return userNameTextBox;
	}

	public QAFWebElement getPasswordTextBox() {
		return passwordTextBox;
	}

	public QAFWebElement getLoginButton() {
		return loginButton;
	}

}
