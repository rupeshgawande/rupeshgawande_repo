package com.qmetry.qaf.example.steps;

import java.awt.AWTException;
import java.util.Properties;

import com.qmetry.qaf.automation.step.CommonStep;
import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.example.pages.ExpenceReimbursmentPage;
import com.qmetry.qaf.example.utilities.Utilities;

public class ExpenceReimbursmentStep {
	Properties prop = new Properties();

	ExpenceReimbursmentPage erp = new ExpenceReimbursmentPage();
	Utilities util = new Utilities();

	@QAFTestStep(description = "User nevigate to {0} and {1} Menu")
	public void nevigateToMenuAndSubMenu(String menu, String subMenu) {

		util.pageLoader();
		util.clickUsingJavaScript(erp.getNevigationButton());
		erp.clickOnMenuAndSubMenu(menu, subMenu);
	}

	@QAFTestStep(description = "User click on New Expence Button and browse button to attach file")
	public void uploadFile() throws AWTException {
		util.pageLoader();
		erp.getNewExpenceButton().click();
		util.pageLoader();
		util.uploadFile(erp.getBrowseButton(), "C:\\Users\\rupesh.gawande\\Desktop\\TestCase_Details");

	}

}
