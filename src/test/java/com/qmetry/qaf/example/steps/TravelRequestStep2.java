package com.qmetry.qaf.example.steps;

import org.hamcrest.Matchers;

import com.qmetry.qaf.automation.step.CommonStep;
import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.util.Validator;
import com.qmetry.qaf.example.pages.TravelRequestPage;
import com.qmetry.qaf.example.utilities.Utilities;

public class TravelRequestStep2 {
	TravelRequestPage trp = new TravelRequestPage();
	Utilities util = new Utilities();

	@QAFTestStep(description = "User click on view all links")
	public void clickOnViewAllLink() {
		util.clickUsingJavaScript(trp.getSwitchView());

	}

	@QAFTestStep(description = "According to the tabs request page should open")
	public void clickOnTabsAndVerifyPage() {
		util.pageLoader();
		util.clickUsingJavaScript(trp.getViewAllButton());
		trp.getTeamlLeaveList().waitForVisible();
		Validator.verifyThat(" verify the tab leave", trp.getTeamlLeaveList().getText(),
				Matchers.containsString("Team Leave List"));

		util.pageLoader();
		trp.getNevigateBack();
		util.pageLoader();
		util.clickUsingJavaScript(trp.getButtonExpenseReq());
		CommonStep.click("link.ViewAll.loc");
		util.pageLoader();

		Validator.verifyThat(" verify the tab Expense Request", trp.getTeamReimbursementList().getText(),
				Matchers.containsString("Team Reimbursement List"));

		util.pageLoader();
		trp.getNevigateBack();

		util.pageLoader();
		util.clickUsingJavaScript(trp.getTravelRequest());
		CommonStep.click("link.ViewAll.loc");
		util.pageLoader();
		Validator.verifyThat("verify the tab Travel Request", trp.getTravelRequestText().getText(),
				Matchers.containsString("Travel Requests"));

	}

}
