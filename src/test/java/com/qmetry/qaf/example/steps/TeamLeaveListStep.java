package com.qmetry.qaf.example.steps;

import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.example.pages.CommanPage;
import com.qmetry.qaf.example.pages.ExpenceReimbursmentPage;
import com.qmetry.qaf.example.pages.TeamLeaveListPage;
import com.qmetry.qaf.example.utilities.Utilities;

public class TeamLeaveListStep {

	ExpenceReimbursmentPage erp = new ExpenceReimbursmentPage();
	TeamLeaveListPage tlp = new TeamLeaveListPage();
	Utilities util = new Utilities();
	CommanPage commanPage = new CommanPage();

	@QAFTestStep(description = "user switch to manager view and neviagate to the {0} module and {1}")
	public void nevigateToLeaveModulesssss(String menu, String subMenu) {
		util.pageLoader();
		tlp.getManagerViewSwitch().click();
		util.clickUsingJavaScript(tlp.getNevigationButton());
		util.pageLoader();
		erp.clickOnMenuAndSubMenu(menu, subMenu);
		util.clickUsingJavaScript(tlp.getTeamlLeaveList());
		util.pageLoader();

	}

	@QAFTestStep(description = "verify the table headers {0} and {1}")
	public void verifyTableHeaders(String data, String loc) {
		commanPage.verifyAllFields(data, loc);

	}

	@QAFTestStep(description = "verify buttons {0} and {1}")
	public void verifyButtons(String loc, String buttonName) {

		util.scrollUptoAxis("200", "700");
		commanPage.verifyButtons(loc, buttonName);
	}

}
