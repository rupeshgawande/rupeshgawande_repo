package com.qmetry.qaf.example.steps;

import org.hamcrest.Matchers;

import com.qmetry.qaf.automation.step.CommonStep;
import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.util.Validator;
import com.qmetry.qaf.example.pages.HomePage;
import com.qmetry.qaf.example.pages.NestLoginPage;
import com.qmetry.qaf.example.utilities.Utilities;

public class NestLoginPageStep extends WebDriverBaseTestPage<WebDriverTestPage> {
	Utilities util = new Utilities();
	HomePage hp = new HomePage();
	NestLoginPage nestloginPage = new NestLoginPage();

	@Override
	protected void openPage(PageLocator arg0, Object... arg1) {

	}

	@QAFTestStep(description = "user launches the application")
	public void userLaunchesTheApplication() {
		driver.get("/");
	}

	@QAFTestStep(description = "user enters username as {0} and password as {1}")
	public void userEntersUsernameAndPasswordAs(String userName, String passWord) {
		CommonStep.sendKeys(userName, "text.username.login.loc");
		CommonStep.sendKeys(passWord, "text.password.login.loc");
	}

	@QAFTestStep(description = "clicks on login button")
	public void clicksOnLoginButton() {
		CommonStep.click("button.Login.loc");

	}

	@QAFTestStep(description = "User logged in successfully and user gets a message {0}")
	public void userLoggedInSuccessfully(String homePageTitle) {

		Validator.verifyThat(driver.getTitle(), Matchers.containsString(homePageTitle));
	}

	@QAFTestStep(description = "user click on logout icon and user nevigated to login page")
	public void userClickedOnLogoutIcon() {

		hp.getLogOutIconButon().waitForVisible();
		util.clickUsingJavaScript(hp.getLogOutIconButon());
	}

	@QAFTestStep(description = "User will not logged in successfully and user gets a message {0}")
	public void userNotLoggedInAndGetsMessage(String message) {

		Validator.verifyThat(" username and password is blank", CommonStep.getText("login.error.msg.loc"),
				Matchers.containsString(message));

	}

}
