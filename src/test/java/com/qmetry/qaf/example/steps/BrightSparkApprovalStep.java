package com.qmetry.qaf.example.steps;

import org.openqa.selenium.Keys;

import com.qmetry.qaf.automation.step.CommonStep;
import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.util.Validator;
import com.qmetry.qaf.example.pages.BrightSparkApprovalPage;
import com.qmetry.qaf.example.pages.ExpenceReimbursmentPage;
import com.qmetry.qaf.example.pages.HomePage;
import com.qmetry.qaf.example.pages.NestLoginPage;
import com.qmetry.qaf.example.pages.TravelRequestPage;
import com.qmetry.qaf.example.utilities.Utilities;

public class BrightSparkApprovalStep {

	NestLoginPage nestLogin = new NestLoginPage();
	Utilities util = new Utilities();
	BrightSparkApprovalPage brightSparkApprovalPage = new BrightSparkApprovalPage();
	ExpenceReimbursmentPage erp = new ExpenceReimbursmentPage();
	HomePage homePage = new HomePage();
	TravelRequestPage trp = new TravelRequestPage();

	@QAFTestStep(description = "Bright Spark Menu and User nominate the employee and fill the fields and click on post button")
	public void nominateEmployee() {
		util.pageLoader();
		util.clickUsingJavaScript(brightSparkApprovalPage.getNominateMenu());
		util.clickUsingJavaScript(brightSparkApprovalPage.getBrightSpark());
		util.pageLoader();
		brightSparkApprovalPage.getCardFirst().click();

		util.selectDropDownMenu(brightSparkApprovalPage.getDropdownNominee(), "Rohit Baronia");

		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		util.pageLoader();

		util.selectDropDownMenu(brightSparkApprovalPage.getDropdownProject(), "Test Project 5");

		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		util.pageLoader();
		brightSparkApprovalPage.getTextAreaChalngeSituation().sendKeys("ok");

		brightSparkApprovalPage.getTextAreaSolutionsProvided().sendKeys("lets");

		brightSparkApprovalPage.getTextAreaBenefitsOcured().sendKeys("work");

		brightSparkApprovalPage.getTextAreaRationale().sendKeys("post");

		util.scrollUptoElement(brightSparkApprovalPage.getButtonPost());

		util.clickUsingJavaScript(brightSparkApprovalPage.getButtonPost());
		util.pageLoader();

		brightSparkApprovalPage.getButtonLogoutImage().waitForVisible();
		util.clickUsingJavaScript(brightSparkApprovalPage.getButtonLogoutImage());

	}

	@QAFTestStep(description = "User nevigate to {0} and {1} Menu and more")
	public void switchManagerView(String menu, String subMenu) {
		util.pageLoader();
		homePage.getManageralViewSwitchButton().waitForVisible();
		util.clickUsingJavaScript(trp.getSwitchView());
		util.pageLoader();
		CommonStep.click("homepage.nevigation.button.loc");
		erp.clickOnMenuAndSubMenu(menu, subMenu);
		util.clickUsingJavaScript(brightSparkApprovalPage.getrRRequestsMenu());
		util.pageLoader();
		brightSparkApprovalPage.getTableDataBrightSpark().click();
		util.pageLoader();
		util.scrollUptoElement(brightSparkApprovalPage.getRejectButton());

	}

	@QAFTestStep(description = "verify bright spark request With Buttons")
	public void verifyRequestWithButtons() {
		Validator.verifyTrue(brightSparkApprovalPage.getApproveButton().isDisplayed(), "button not displayed",
				"button displayed");
		Validator.verifyTrue(brightSparkApprovalPage.getRejectButton().isDisplayed(), "button not displayed",
				"button displayed");

	}

}
