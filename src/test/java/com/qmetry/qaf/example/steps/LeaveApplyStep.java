package com.qmetry.qaf.example.steps;

import com.qmetry.qaf.automation.core.ConfigurationManager;
import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.example.pages.HomePage;
import com.qmetry.qaf.example.pages.LeaveApplyPage;
import com.qmetry.qaf.example.utilities.Utilities;

public class LeaveApplyStep {
	HomePage homePage = new HomePage();
	LeaveApplyPage lap = new LeaveApplyPage();
	Utilities util = new Utilities();

	@QAFTestStep(description = "user nevigate to Leave module")
	public void nevigateToLeaveModule() {
		util.pageLoader();
		homePage.getNevigationButton().click();
		lap.getNevigationArrow().click();
		lap.getApplyLeaveMenu().click();

	}

	@QAFTestStep(description = "select type of leave and leave date")
	public void selectLeaveType() {
		util.pageLoader();
		util.selectDropDownMenu(lap.getDropDown(), "Family Function");
		util.calDate(ConfigurationManager.getBundle().getProperty("dateLast"), lap.beforeXpath, lap.afterXpath,
				lap.getDropdownImage());

	}

	@QAFTestStep(description = "select the Leave reason and select full day Leave and click on Apply button")
	public void applyForLeave() {

		lap.getApply().click();
	}

}
