package com.qmetry.qaf.example.steps;



import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;
import com.qmetry.qaf.automation.util.Validator;
import com.qmetry.qaf.example.pages.HomePage;
import com.qmetry.qaf.example.pages.TravelRequestPage;
import com.qmetry.qaf.example.utilities.Utilities;

public class TravelRequestStep {

	HomePage homePage = new HomePage();
	Utilities util;
	TravelRequestPage trp;

	
	@QAFTestStep(description = "user click on Travel tab and User click on view Icon")
	public void click_On_AnyOption() {
		trp = new TravelRequestPage();
		util = new Utilities();

		
		homePage.getManageralViewSwitchButton().waitForVisible();
		util.clickUsingJavaScript(trp.getSwitchView());
        util.pageLoader();
		trp.getTravelRequest().waitForVisible();
		QAFExtendedWebElement travelRequest = new QAFExtendedWebElement("btn.travelRequest.click.loc");
		util.clickUsingJavaScript(travelRequest);
		util.moveToElement(trp.getMoveToAreaTravelRequests());
		trp.getViewEyeButton().waitForVisible();
		util.clickUsingJavaScript(trp.getViewEyeButton());
		util.pageLoader();

	}

	@QAFTestStep(description = "verify the {0} button available")
	public void verify_ApproveButtons_Available(String buttonName) 
	{
		Boolean flag=trp.getAllButtons().verifyPresent();
		Validator.verifyTrue(flag, "Buttons not present", "Buttons present");
	}
	
}
