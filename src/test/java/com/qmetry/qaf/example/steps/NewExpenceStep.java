package com.qmetry.qaf.example.steps;

import org.hamcrest.Matchers;
import org.openqa.selenium.Keys;
import org.testng.Reporter;

import com.qmetry.qaf.automation.core.ConfigurationManager;
import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.automation.util.Validator;
import com.qmetry.qaf.example.pages.LeaveApplyPage;
import com.qmetry.qaf.example.pages.NewExpencePage;
import com.qmetry.qaf.example.utilities.Utilities;

public class NewExpenceStep {

	NewExpencePage newExpencePage = new NewExpencePage();
	Utilities util = new Utilities();
	LeaveApplyPage leaveApplyPage = new LeaveApplyPage();

	@QAFTestStep(description = "user click on New Expence Button and fill invalid data in fields")
	public void fillInvalidData() {
		util.pageLoader();
		util.clickUsingJavaScript(newExpencePage.getNewExpenceButton());
		util.pageLoader();
		newExpencePage.getTitleTextBox().sendKeys("2435");
		newExpencePage.getColorCalender().sendKeys("##$$%%");

		util.selectDropDownMenu(newExpencePage.getProjectSelectBox(), "Other");

		newExpencePage.getProjectSelectBox().sendKeys(Keys.DOWN);
		newExpencePage.getProjectSelectBox().sendKeys(Keys.ENTER);

		util.clickUsingJavaScript(newExpencePage.getProjectTextBox());
		newExpencePage.getProjectTextBox().sendKeys("API");

		newExpencePage.getExpencCatSelectBOX().sendKeys(Keys.DOWN);
		newExpencePage.getExpencCatSelectBOX().sendKeys(Keys.ENTER);

		newExpencePage.getCurrencyTextBox().sendKeys("6000");

	}

	@QAFTestStep(description = "click on submit button and verify the error color")
	public void verifyErrorColor() {
		util.clickUsingJavaScript(newExpencePage.getSubmitButton());
		String value = newExpencePage.getColorCalender().getCssValue("color");
		Validator.verifyThat("verifying color", value, Matchers.containsString("rgba(85, 85, 85, 1)"));

	}
}
