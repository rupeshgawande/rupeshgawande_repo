package com.qmetry.qaf.example.steps;

import java.io.IOException;
import java.util.Properties;

import org.hamcrest.Matchers;

import com.qmetry.qaf.automation.core.ConfigurationManager;
import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.util.Validator;
import com.qmetry.qaf.example.pages.HomePage;
import com.qmetry.qaf.example.pages.LeaveApplyHolidayPage;
import com.qmetry.qaf.example.pages.LeaveApplyPage;
import com.qmetry.qaf.example.utilities.Utilities;

public class LeaveApplyHolidaySteps {

	HomePage homePage = new HomePage();
	Utilities util = new Utilities();
	LeaveApplyPage leaveApplyPage = new LeaveApplyPage();
	LeaveApplyHolidayPage leaveHolidayPage = new LeaveApplyHolidayPage();

	@QAFTestStep(description = "select type of leave and select the Leave reason")
	public void leaveTypeAndLeaveReason() {
		util.pageLoader();
		util.selectDropDownMenu(leaveApplyPage.getDropDown(), "Family Function");
	}

	@QAFTestStep(description = "select date range that includes Holiday and click on Apply button")
	public void selectDateRangeAndApply() throws IOException {

		util.calDate(ConfigurationManager.getBundle().getProperty("dateFirst"), leaveApplyPage.beforeXpath,
				leaveApplyPage.afterXpath, leaveApplyPage.getDropdownImage());
		util.calDate(ConfigurationManager.getBundle().getProperty("dateLast"), leaveApplyPage.beforeXpath,
				leaveApplyPage.afterXpath, leaveApplyPage.getDropdownImage());
		util.scrollUptoElement(leaveApplyPage.getApply());
		util.clickUsingJavaScript(leaveApplyPage.getApply());

		Validator.verifyThat("verifying the applied leave value", leaveHolidayPage.getLeaveValueText().getText(),
				Matchers.containsString("3"));
	}
}
