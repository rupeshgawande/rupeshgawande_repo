package com.qmetry.qaf.example.utilities;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Properties;

import org.hamcrest.Matchers;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.WebDriver.Navigation;
import org.openqa.selenium.WebDriver.Options;
import org.openqa.selenium.WebDriver.TargetLocator;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.qmetry.qaf.automation.step.CommonStep;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.WebDriverTestBase;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebDriver;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.automation.util.Reporter;
import com.qmetry.qaf.automation.util.Validator;
import com.qmetry.qaf.example.pages.HomePage;
import com.sun.java.swing.plaf.windows.resources.windows;

public class Utilities {
	HomePage hp = new HomePage();

	public void pageLoader() {
		CommonStep.waitForNotVisible("Login.Loader.img", 120);
		hp.getLogOutIconButon().waitForEnabled();

	}

	public void loader() {
		CommonStep.waitForNotVisible("Login.Loader.img", 120);
	}

	public void clickUsingJavaScript(WebElement element) {
		JavascriptExecutor executor = (JavascriptExecutor) new WebDriverTestBase().getDriver();
		executor.executeScript("arguments[0].click();", element);

	}

	public void scrollUptoElement(WebElement element) {
		JavascriptExecutor executor = (JavascriptExecutor) new WebDriverTestBase().getDriver();
		executor.executeScript("arguments[0].scrollIntoView();", element);

	}

	public void scrollDownFull() {
		JavascriptExecutor executor = (JavascriptExecutor) new WebDriverTestBase().getDriver();
		executor.executeScript("window.scrollTo(0,document.body.scrollHeight)");
	}

	public void scrollUptoAxis(String x, String y) {
		((JavascriptExecutor) new WebDriverTestBase().getDriver())
				.executeScript("window.scrollBy(" + x + "," + y + ")");

	}

	public void uploadFile(QAFWebElement element, String filePath) throws AWTException {

		element.click();
		Robot robot = new Robot();

		StringSelection str = new StringSelection(filePath);
		Toolkit.getDefaultToolkit().getSystemClipboard().setContents(str, null);
		robot.keyPress(KeyEvent.VK_ENTER);
		robot.keyRelease(KeyEvent.VK_ENTER);
		robot.keyPress(KeyEvent.VK_CONTROL);
		robot.keyPress(KeyEvent.VK_V);
		robot.keyRelease(KeyEvent.VK_CONTROL);
		robot.keyRelease(KeyEvent.VK_V);
		robot.setAutoDelay(2000);
		robot.keyPress(KeyEvent.VK_ENTER);
		robot.keyRelease(KeyEvent.VK_ENTER);
	}

	public void clickMenuAndSubmenu(QAFWebElement menu, QAFWebElement subMenu) {
		clickUsingJavaScript(menu);
		clickUsingJavaScript(subMenu);
	}

	public void selectDropDownMenu(QAFWebElement elementDrop, String option) {
		elementDrop.click();
		clickUsingJavaScript(
				new WebDriverTestBase().getDriver().findElement(By.xpath("//span[text()='" + option + "']")));
	}

	public void moveToElement(QAFWebElement targetElement) 
	{
		Actions action=new Actions(new WebDriverTestBase().getDriver());
		action.moveToElement(targetElement).build().perform();
	}
	
	public void doubleClickOnElement(QAFWebElement targetElement) 
	{
		Actions action=new Actions(new WebDriverTestBase().getDriver());
		action.doubleClick(targetElement).build().perform();
	}
	
	
	
	public void calDate(Object object, String beforeXpath, String afterXpath, QAFWebElement element) {
		// String date="06-July-2018";
		String[] dateArray = ((String) object).split("-");
		String day = dateArray[0];
		String month = dateArray[1];
		String year = dateArray[2];

		clickUsingJavaScript(element);

		final int total_week_days = 7;
		boolean flag = false;
		String dayValue = null;
		for (int rowNum = 2; rowNum <= 7; rowNum++) {
			for (int colNum = 1; colNum < total_week_days; colNum++) {

				try {
					dayValue = new WebDriverTestBase().getDriver()
							.findElement(By.xpath(beforeXpath + rowNum + afterXpath + colNum + "]")).getText();
				} catch (NoSuchElementException e) {
					Reporter.log("please enter correct value");
					flag = false;
					break;
				}
				Reporter.log("day value is :" + dayValue);
				if (dayValue.equals(day)) {
					new WebDriverTestBase().getDriver()
							.findElement(By.xpath(beforeXpath + rowNum + afterXpath + colNum + "]")).click();
					flag = true;
					break;

				}
			}
			if (flag) {
				break;
			}
		}

	}
}

